const router = require('express').Router();
const register = require('../controllers/auth/register')

const sendJWT = require('../controllers/auth/signJWT')
const authorize = require('../controllers/auth/authorize')

const passport = require('passport')
require('../controllers/auth/strategy.google')
require('../controllers/auth/strategy.local')

router
    .route('/google')
    .get(passport.authenticate('google', { scope: ['profile'] }))

router
    .route('/google/callback')
    .get(
        passport.authenticate('google', { failureRedirect: '/login' }),
        sendJWT
    )

router
    .route('/local')
    .post(
        passport.authenticate('local', { failureRedirect: '/login' }),
        sendJWT
    )

router
    .route('/local/register')
    .post(
        register,
        sendJWT
    )

router
    .route('/')
    .get(
        authorize,
        (req, res) => {
            res.json({ ok: true })
        }
    )

module.exports = router