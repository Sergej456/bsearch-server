const router = require('express').Router();

const ur = require('../controllers/controller.ur')

router
    .route('/ur')
    .get(ur.get)

module.exports = router