const mongoose = require('mongoose')
const enc = require('../helpers/encrypt')

const UserSchema = new mongoose.Schema({
  id: {
    type: String,
    required: true,
    unique: true
  },
  name: {
    type: String,
    default: 'No Name'
  },
  password_hash: {
    type: String,
    default: null
  }
})

module.exports = class UserModel {
  constructor() {
    this.userModel = mongoose.model('UserModelCollection', UserSchema)
  }

  registerUser (id, name, password) {
    console.log(id, name, password)
    return new Promise(async (resolve, reject) => {
      enc.cryptPassword(password).then(hash => {
        console.log(hash)
        this.userModel.create({ id, name, password_hash: hash})
          .then(resBD => {
            resBD.password_hash = undefined;
            resolve(resBD)
          })
          .catch(err => reject(`Registration error: ${err.message}`))
      })
    })
  }

  getUser ({ id, password }) {
    return new Promise((resolve, reject) => {
      this.userModel
        .findOne({ id })
        .then(resBD => {
          enc.comparePassword(password, resBD.password_hash)
            .then(isPasswordMatch => {
              if (isPasswordMatch) {
                resBD.password_hash = undefined;
                resolve(resBD)
              }
              else reject(`Login local error: ${err.message}`)
            })
            .catch(err => reject(`Login local error: ${err.message}`))
        })
        .catch(err => reject(`Login local error: ${err.message}`))
    })
  }

  getUserSocial ({ id, name }) {
    return new Promise(async (resolve, reject) => {
        this.userModel
          .findOne({ id })
          .then(resBD => {
            if (resBD) {
              resBD.password_hash = undefined;
              resolve(resBD)
            }
            else this.userModel.create({ id, name })
              .then(resBD => {
                resBD.password_hash = undefined;
                resolve(resBD)
              })
          })
          .catch(err => reject(`Login social error: ${err.message}`))
      })
  }

}
