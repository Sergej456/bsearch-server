const fs = require('fs')
const CsvReadableStream = require('csv-reader')
const axios = require('axios')
const path = require('path')
const Cron = require('cron').CronJob
const colors = require('colors')

let registerArray = []
let ppiPublicArray = []
let ppiDelegatedArray = []
let areaOfActivityArray = []

let toSync = {}

exports.sync = (register) => {
    toSync[register] = true
    return new Promise(async (resolve, reject) => {
        let url = 'http://dati.ur.gov.lv/register/register.csv'
        let file = 'register.csv'

        switch (register) {
            case 'register': 
                url = 'http://dati.ur.gov.lv/register/register.csv'
                file = 'register.csv'
                break
            case 'ppi_public':
                url = 'http://dati.ur.gov.lv/register/ppi_public_persons_institutions.csv'
                file = 'ppi_public.csv'
                break
            case 'ppi_delegated':
                url = 'http://dati.ur.gov.lv/register/ppi_delegated_entities.csv'
                file = 'ppi_delegated.csv'
                break
            case 'area_of_activity':
                url = 'http://dati.ur.gov.lv/register/areas_of_activity_of_associations_foundations.csv'
                file = 'area_of_activity.csv'
        }
        const fileStream = await axios({
            method: 'get',
            url,
            responseType: 'stream'
        }).then(response => {
            return response.data.pipe(fs.createWriteStream(path.join(__dirname, file)))
        })
        .catch(reject)

        fileStream.on('finish', () => resolve(file))
    })
}

exports.prepareArray = (inputFile) => {
    return new Promise((resolve, reject) => {
        let tempArray = []
        const inputStream = fs.createReadStream(path.join(__dirname, inputFile), 'utf8')
        inputStream
        .pipe(CsvReadableStream({ 
            parseNumbers: false, 
            parseBooleans: false, 
            trim: true, 
            delimiter: ';' 
        }))
        .on('data', row => {
            tempArray.push(row)
        })
        .on('end', () => {
            switch (inputFile) {
                case 'register.csv': registerArray = [...tempArray]; break;
                case 'ppi_public.csv': ppiPublicArray = [...tempArray]; break;
                case 'ppi_delegateg.csv': ppiDelegatedArray = [...tempArray]; break;
                case 'area_of_activity.csv': areaOfActivityArray = [...tempArray]; break;
            }
            resolve()
        })
        .on('error', reject)
    })
}

exports.search = (value, type, one = false) => {
    const handler = (el, index) => {
        if (!index) return true
        let boolean = false
        for(let i = 0; i < el.length; i++) {
            if (el[i].toLowerCase().includes(value.toLowerCase())) boolean = true
        }
        return boolean
    }
    if (!one) {
        switch (type) {
            case 'register': return registerArray.filter(handler);
            case 'ppi_public': return ppiPublicArray.filter(handler);
            case 'ppi_delegated': return ppiDelegatedArray.filter(handler);
            case 'area_of_activity': return areaOfActivityArray.filter(handler);
        }
    } else {
        switch (type) {
            case 'register': return registerArray.filter(handler).splice(0, 2);
            case 'ppi_public': return ppiPublicArray.filter(handler).splice(0, 2);
            case 'ppi_delegated': return ppiDelegatedArray.filter(handler).splice(0, 2);
            case 'area_of_activity': return areaOfActivityArray.filter(handler).splice(0, 2);
        }
    }
}

exports.init = (register) => {
    return new Promise( resolve => {
        this.sync(register)
            .then(file => this.prepareArray(file))
            .then(resolve)
    })
}

const cron = new Cron('0 0 * * *', async () => {
    console.log(colors.magenta('UR register sync started'))
    const promiseArray = []
    Object.keys(toSync).forEach(el => {
        if (toSync[el]) {
            promiseArray.push(this.sync(el))
        }
    })
    const ready = await Promise.all(promiseArray)
    console.log(colors.green('UR register sync finished', ready))
}, null, true, 'America/Los_Angeles')