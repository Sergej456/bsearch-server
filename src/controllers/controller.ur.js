const ur = require('./ur/ur.worker')
const response = require('../helpers/response')
let ready = false

ur.init('register')
    .then(() => { console.log('register ready'); return ur.init('ppi_public')})
    .then(() => { console.log('ppi_public ready'); return ur.init('ppi_delegated')})
    .then(() => { console.log('ppi_delegated ready'); return ur.init('area_of_activity')})
    .then(() => { console.log('area_of_activity ready'); ready = true })
    .catch(err => console.log(err))

exports.get = (req, res) => {
    if (!ready) response(res, { status: 'Service Unavailable', error: new Error('URRegisterNotReady') })
    else if (!(req && req.query && req.query.search && req.query.type)) response(res, { status: 'Bad Request', error: new Error('NoData') })
    else response(res, {
        status: 'OK',
        data: ur.search(req.query.search, req.query.type, !!(req.query.one === 'true'))
    })
}