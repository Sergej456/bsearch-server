const fs = require('fs')
const ejs = require('ejs')
const path = require('path')
const axios = require('axios')
const xml2js = require('xml2js')

exports.create = (data) => {
    let template = ejs.compile(
        fs.readFileSync(path.join(__dirname, '/xml.template.ejs'), { 
            encoding: 'UTF-8'
        }), {})
    data.params = Object.keys(data.params).map(el => {
        return `<${el}>${data.params[el]}</${el}>`
    })
    return template(data)
}

exports.send = (body) => {
    return new Promise((resolve, reject) => {
        axios.post('https://www.klientuportfelis.lv/api', body, {})
            .then(res => xml2js.parseStringPromise(res.data))
            .catch(err => xml2js.parseStringPromise(err.response.data))
            .then(resolve)
    })
}