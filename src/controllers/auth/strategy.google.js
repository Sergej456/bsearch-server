const UserModel = require('../../database/user')
const user = new UserModel();

const passport = require('passport')
var GoogleStrategy = require('passport-google-oauth20').Strategy;

const verifyHandler = function (accessToken, refreshToken, profile, cb, done) {
  const data = { id: cb.id + cb.provider, name: cb.displayName }
  user.getUserSocial(data)
    .then(data => {
        console.log(data)
        done(null, data)
    })
    .catch(err => {
        console.log(err)
        done(null, null)
    })
};


passport.use(new GoogleStrategy({
    clientID: '464270442808-pafcaui7nmbasvqlm48199mbr7v54c63.apps.googleusercontent.com',
    clientSecret: '5Hfvr1Gm_xDkAuTNlyWBW0YR',
    callbackURL: 'http://localhost:8080/api/auth/google/callback'
  }, verifyHandler))