const UserModel = require('../../database/user')
const user = new UserModel();

const passport = require('passport')
var LocalStrategy = require('passport-local').Strategy;

const verifyHandler = function (username, password, done) {
  const data = { id: username+'local', name: username, password }
  user.getUser(data)
    .then(resBD => {
        done(null, resBD)
    })
    .catch(err => {
        done(null, null)
    })
};

passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    session: false
},
    verifyHandler)
)