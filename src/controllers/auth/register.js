const UserModel = require('../../database/user')
const user = new UserModel();

const validator = require('validator')

const response = require('../../helpers/response')

module.exports = (req, res, next) => {
    if (req && req.body && req.body.email && req.body.password) {
      req.body.email = req.body.email.toLowerCase()
      if (!validator.isEmail(req.body.email)) return response(res, { status: 'Bad Request', error: new Error('incorrectEmailFormat') })
      if (req.body.password.length < 8) return response(res, { status: 'Bad Request', error: new Error('shortPassword') })
      user.registerUser(req.body.email+'local', req.body.email, req.body.password)
        .then(resBD => {
            req.user = resBD
            next()
        })
        .catch(err => {
            console.log(err)
            response(res, { status: 'Bad Request', error: err })
        })
    } else response(res, { status: 'Bad Request', error: new Error('no Data') })
  }